(*
  Copyright © 2009 Stéphane Glondu <steph@glondu.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Dependencies: wget, bzip2, ocsigen-dev, libocamlgraph-ocaml-dev.
*)

{
  let ocaml_version = "3.11.1"

  let mirror = "http://ftp.debian.org/debian"
  let suite = "unstable"
  let sections = ["main"; "contrib"; "non-free"]

  let dom_mail = "Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>"

  (** Basename of all files handled by this script *)
  let basename = "ocaml_transition_monitor"

  let src_webbrowse_url = "http://git.debian.org/?p=pkg-ocaml-maint/tools/ocaml-transition-monitor.git;a=blob;f=ocaml_transition_monitor.mll;hb=HEAD"

  let architectures =
    [ "alpha"; "amd64"; "armel"; "hppa"; "i386"; "ia64"; "kfreebsd-amd64"; "kfreebsd-i386"; "mips"; "mipsel"; "powerpc"; "s390"; "sparc" ]
  let all_architectures = String.concat " " architectures

  module S = Set.Make(String)
  module M = Map.Make(String)

  open Printf

  type source_package = {
    sname: string;
    smaint: string;
    sdeps: string list;
    sbins : string list;
    sversion: string;
  }
  type binary_package = {
    bname: string;
    bdeps: string list;
    bsrc: string;
    bversion: string;
    bnmu: int;
  }

  let headers_to_keep =
    [ "Package"; "Binary"; "Version"; "Build-Depends"; "Build-Depends-Indep"; "Depends"; "Architecture"; "Source"; "Provides" ]

  type status = Unknown | Up_to_date | Outdated
  let string_of_status = function
    | Unknown -> " "
    | Up_to_date -> "✔"
    | Outdated -> "✘"
  let class_of_status = function
    | Unknown -> "unknown"
    | Up_to_date -> "good"
    | Outdated -> "bad"

  let skip_download = ref false
  let quiet_mode = ref false
  let use_cache = ref false

  let progress x =
    if !quiet_mode then ifprintf stderr x else fprintf stderr x

  let find x xs =
    try M.find x xs
    with Not_found -> invalid_arg ("Not_found: "^x)

  module G = struct
    module V = struct
      type t = string
      let equal = (=)
      let hash = Hashtbl.hash
    end
    type t = S.t M.t * S.t M.t
    let iter_vertex f (_, deps) = M.iter (fun k _ -> f k) deps
    let iter_succ f (deps, _) pkg = S.iter f (find pkg deps)
    let in_degree (_, rdeps) pkg = S.cardinal (find pkg rdeps)
  end

  module Topological = Graph.Topological.Make(G)

  let with_in_file file f =
    let chan = open_in_bin file in
    try
      let res = f chan in
      close_in chan; res
    with e -> close_in chan; raise e

  let with_out_file file f =
    let chan = open_out_bin file in
    try
      let res = f chan in
      close_out chan; res
    with e -> close_out chan; raise e

  let get_rfc2822_date () =
    let chan = Unix.open_process_in "date -R" in
    let r = input_line chan in
    match Unix.close_process_in chan with
      | Unix.WEXITED 0 -> r
      | _ -> failwith "unexpected return of date"

  let list_iteri f xs =
    let rec aux i = function
      | [] -> ()
      | x::xs -> f i x; aux (i+1) xs
    in aux 0 xs

  let list_rev_mapi f xs =
    let rec aux i accu = function
      | [] -> accu
      | x::xs -> aux (i+1) ((f i x)::accu) xs
    in aux 0 [] xs
}

let name = ['A'-'Z' 'a'-'z' '0'-'9' '-' '.' ':' '~' '+']+

rule entry accu = parse
  | ([^':' '\n']+ as header) ":"
      {
        match header with
          | "Maintainer" ->
              entry ((header, [maintainer lexbuf])::accu) lexbuf
          | s when List.mem s headers_to_keep ->
              entry ((header, values [] lexbuf)::accu) lexbuf
          | _ ->
              (skip lexbuf; entry accu lexbuf)
      }
  | eof | '\n' { if accu = [] then raise End_of_file else accu }
and maintainer = parse
  | ' '+ ([^'\n']+ as email) '\n' { email }
and values accu = parse
  | name as name { values (name::accu) lexbuf }
  | [' ' ',' '|']+ { values accu lexbuf }
  | '(' [^')']* ')' | "\n " | '[' [^']']* ']'
        { values accu lexbuf }
  | "\n " { values accu lexbuf }
  | "\n" { accu }
and skip = parse
  | ([^'\n']* "\n ")* [^'\n']* "\n" { () }

{
  let get_one = function
    | [a] -> a
    | x -> invalid_arg (sprintf "[%s] should have exactly one element" (String.concat ", " x))

  let assoc ?default x xs =
    try List.assoc x xs
    with Not_found -> match default with
      | None -> invalid_arg ("Not_found: "^x)
      | Some y -> y

  (**
     @param channel a Sources file
     @return a [source_package M.t] indexed by source package names
  *)
  let parse_source channel =
    let lexbuf = Lexing.from_channel channel in
    let result = ref M.empty in
    let rec aux () =
      let entry = entry [] lexbuf in
      let name = get_one (assoc "Package" entry) in
      let maint = get_one (assoc "Maintainer" entry) in
      let entry = {
        sname = name;
        smaint = maint;
        sdeps = (assoc ~default:[] "Build-Depends" entry) @ (assoc ~default:[] "Build-Depends-Indep" entry);
        sbins = assoc "Binary" entry;
        sversion = get_one (assoc "Version" entry);
      } in
      result := M.add name entry !result;
      aux ()
    in try aux () with End_of_file -> !result

  (**
     @param src a [source_package M.t]
     @param bin a [string M.t] mapping binary packages to their source package
     @return a [S.t M.t] mapping source packages to their build-dependencies
             in terms of source packages
  *)
  let get_dep_graph src bin =
    M.mapi
      (fun k pkg ->
         List.fold_left
           (fun accu dep ->
              try S.add (M.find dep bin) accu with Not_found -> accu)
           S.empty pkg.sdeps)
      src

  let invert_dep_graph src =
    M.mapi
      (fun pkg _ ->
         M.fold
           (fun k deps accu -> if S.mem pkg deps then S.add k accu else accu)
           src S.empty)
      src

  (**
     @param dgraph [get_dep_graph] output
     @return a [string list list] with topologically sorted source packages,
             grouped by dependency level
  *)
  let topo_split dgraph =
    let inverted = invert_dep_graph dgraph in
    let (a, b) =
      Topological.fold
        (fun pkg (local, accu) ->
           if S.exists (fun x -> S.mem x local) (find pkg dgraph) then
             (* already a dependency in this level -> switch to next level *)
             (S.add pkg S.empty, local::accu)
           else
             (* stay on the same level *)
             (S.add pkg local, accu))
        (inverted, dgraph)
        (S.empty, [])
    in
    List.rev_map S.elements (a::b)

  (**
     @param channel a Packages file
     @return a [binary_package M.t] indexed by binary package names
  *)
  let parse_binary channel =
    let lexbuf = Lexing.from_channel channel in
    let result = ref M.empty in
    let rec aux () =
      let entry = entry [] lexbuf in
      let name = get_one (assoc "Package" entry) in
      let entry = {
        bname = name;
        bdeps = (assoc ~default:[] "Depends" entry) @ (assoc ~default:[] "Provides" entry);
        bsrc = get_one (assoc ~default:[name] "Source" entry);
        bversion = get_one (assoc "Version" entry);
        bnmu = 0;
      } in
      result := M.add name entry !result;
      aux ()
    in try aux () with End_of_file -> !result

  let runtime_ocaml_regexp = Str.regexp "^ocaml\\(-base\\)?\\(-nox\\)?-\\([0-9.]+\\)$"
  let build_ocaml_regexp = Str.regexp "^ocaml\\(-nox\\)?$"

  (**
     @param x a binary package name
     @return [Some v] if [x] depends on ABI [v], [None] otherwise
  *)
  let runtime_depends_on_ocaml x =
    let rec aux = function
      | [] -> None
      | x::xs ->
          if Str.string_match runtime_ocaml_regexp x 0 then
            Some (Str.matched_group 3 x)
          else aux xs
    in aux x.bdeps

  let build_depends_on_ocaml x =
    let rec aux = function
      | [] -> false
      | x::xs ->
          if Str.string_match build_ocaml_regexp x 0 then
            true
          else aux xs
    in x.smaint = dom_mail || aux x.sdeps

  let get_binaries arch =
    let file = "Packages."^arch in
    progress "Parsing %s...%!" file;
    let r = with_in_file ("Packages."^arch) parse_binary in
    progress "\n%!"; r

  let get_sources () =
    progress "Parsing Packages.source...%!";
    let sources = with_in_file "Packages.source" parse_source in
    progress "\n%!";
    let sources = M.fold
      (fun k pkg accu ->
         if build_depends_on_ocaml pkg then M.add k pkg accu else accu)
      sources M.empty in
    sources

  let get_package_lists () =
    List.for_all
      (fun arch ->
         let commands = List.map
           (fun section ->
              let url = sprintf "%s/dists/%s/%s/binary-%s/Packages.bz2" mirror suite section arch in
              let cmd = sprintf "{ wget -q -O- '%s' | bzcat >> Packages.new; }" url in
              cmd)
           sections
         in
         let cmd = "rm -f Packages.new && " ^ (String.concat " && " commands) ^ " && mv Packages.new Packages." ^ arch in
         progress "Downloading Packages.%s...%!" arch;
         let r = Sys.command cmd in
         progress "\n%!";
         r = 0)
      architectures
    && (let commands = List.map
          (fun section ->
             let url = sprintf "%s/dists/%s/%s/source/Sources.bz2" mirror suite section in
             let cmd = sprintf "{ wget -q -O- '%s' | bzcat >> Packages.new; }" url in
             cmd)
          sections
        in
        let cmd = "rm -f Packages.new && " ^ (String.concat " && " commands) ^ " && mv Packages.new Packages.source" in
        progress "Downloading Packages.source...%!";
        let r = Sys.command cmd in
        progress "\n%!";
        r = 0)

  let get_binary_status pkg =
    match runtime_depends_on_ocaml pkg with
      | None -> Unknown
      | Some version -> if version = ocaml_version then Up_to_date else Outdated

  (**
     @param binaries a [status M.t] mapping binary packages to their status
     @param source a [source_package]
     @return the worst status among all binary packages of [source]
  *)
  let get_source_status_on_arch binaries source =
    let rec aux accu = function
      | [] -> accu
      | x::xs ->
          let x = try M.find x binaries with Not_found -> Unknown in
          match x with
            | Outdated -> Outdated
            | Up_to_date -> aux Up_to_date xs
            | Unknown -> aux accu xs
    in aux Unknown source.sbins

  let binNMU_regexp = Str.regexp "^\\(.*\\)\\+b\\([0-9]+\\)$"

  let get_binNMU_number_on_arch binaries source =
    let sver = source.sversion in
    let sbins = List.fold_left
      (fun accu x -> try (M.find x binaries)::accu with Not_found -> accu)
      [] source.sbins
    in
    List.fold_left
      (fun accu pkg ->
         let bver = pkg.bversion in
         if Str.string_match binNMU_regexp bver 0 then begin
           let v = Str.matched_group 1 bver in
           if v = sver then
             max accu (int_of_string (Str.matched_group 2 bver))
           else accu
         end else accu)
      0 sbins

  let get_next_binNMU_number xs =
    succ (List.fold_left max 0 xs)

  (**
     @param xs outputs of [get_source_status_on_arch]
     @return the best status among all architectures
  *)
  let get_source_status xs =
    let rec aux accu = function
      | [] -> accu
      | Up_to_date::_ -> Up_to_date
      | Outdated::xs -> aux Outdated xs
      | Unknown::xs -> aux accu xs
    in aux Unknown xs

  let main () =
    let (sources, binaries) =
      let cache = basename^".cache" in
      if !use_cache then begin
        progress "Loading cache...%!";
        let r =  with_in_file cache Marshal.from_channel in
        progress "\n%!"; r
      end else begin
        let x = (get_sources (), List.map get_binaries architectures) in
        with_out_file cache (fun chan -> Marshal.to_channel chan x []);
        x
      end
    in
    let binaries_status = List.map (M.map get_binary_status) binaries in
    let sources_status = M.map
      (fun pkg -> List.map (fun x -> get_source_status_on_arch x pkg) binaries_status)
      sources
    in
    let summary_status = M.map get_source_status sources_status in
    let src_of_bin_map = M.fold
      (fun src spkg accu ->
         List.fold_left
           (fun accu bin -> M.add bin src accu)
           accu spkg.sbins)
      sources M.empty in
    let dep_graph = get_dep_graph sources src_of_bin_map in
    (* a section is a level in the dependency graph *)
    let sections = topo_split dep_graph in
    let date = get_rfc2822_date () in
    progress "Generating XHTML summary page...%!";

    let module XHTML = struct
      open XHTML.M

      let a_link href contents =
        a ~a:[a_href (uri_of_string href)] [pcdata contents]

      let format_package i pkg =
        let bin_statuses = M.find pkg sources_status in
        let all_ok = List.for_all (fun x -> x = Up_to_date || x = Unknown) bin_statuses in
        let src_classes =
          let x = [class_of_status (M.find pkg summary_status); "src"; sprintf "round%d" i] in
          if all_ok then "all_ok"::x else x
        in tr
          (td
             ~a:[a_class src_classes; a_id pkg]
             [a
                ~a:[a_href (uri_of_string ("http://packages.qa.debian.org/"^pkg));
                    a_title
                      (let deps = S.elements (M.find pkg dep_graph) in
                       if deps <> [] then
                         "dependencies: "^(String.concat ", " deps)
                       else
                         "no dependencies");
                   ]
                [pcdata pkg];
              small [
                pcdata " [ ";
                a_link ("http://debian.glondu.net/buildd/package?p="^pkg) "buildd";
                pcdata " ] "
              ];
              small [
                pcdata " ( ";
                a_link
                  (sprintf "http://packages.debian.org/changelogs/pool/main/%c/%s/current/changelog" pkg.[0] pkg)
                  (M.find pkg sources).sversion;
                pcdata " ) ";
              ];
             ])
          (List.map
             (fun x ->
                let x = class_of_status x and xx = string_of_status x
                in td ~a:[a_class [x]] [small [pcdata xx]])
             bin_statuses)

      let format_section i section =
        let thead = tr ~a:[a_id (sprintf "header%d" i)]
          (th [pcdata "source ";
               small [pcdata (sprintf " (round %d)" i)];
              ])
          (List.map (fun arch -> th [small [pcdata arch]]) architectures) in
        thead::(List.map (format_package i) section)

      let summary_contents = List.fold_left
        (fun accu section -> section@accu)
        [] (list_rev_mapi format_section sections)

      let summary = match summary_contents with x::xs -> table x xs | _ -> assert false
      let page_title = "Monitoring OCaml transition to "^ocaml_version

      let footer = [
        p [pcdata "Last generated: ";
           span ~a:[a_class ["timestamp"]] [pcdata date];
           pcdata " (page currently rebuilt every 6 hours).";
           br ();
           pcdata "Contact: ";
           a_link "mailto:steph@glondu.net" "Stéphane Glondu";
           pcdata "; the code that generates this page ";
           a_link src_webbrowse_url "is available";
           pcdata "."; br ();
           pcdata "This page has been type-checked by OCaml using ";
           a_link "http://ocsigen.org/docu/1.1.0/XHTML.M.html" "XHTML.M";
           pcdata " and should be ";
           a_link "http://validator.w3.org/check?uri=referer" "XHTML Valid";
           pcdata "."; br ();
           pcdata "Kudos: Mehdi Dogguy, David Mentré and Stefano Zacchiroli for their remarks and ideas.";
          ]]

      let nb_columns = List.length architectures + 1
      let nb_rounds = List.length sections
      let init_js = sprintf "var nb_columns = %d; var nb_rounds = %d;" nb_columns nb_rounds

      let html = html ~a:[a_xmlns `W3_org_1999_xhtml]
        (head (title (pcdata page_title))
           [link ~a:[a_rel [`Stylesheet]; a_href (uri_of_string (basename^".css"))] ();
            script ~contenttype:"text/javascript" (pcdata init_js);
            script ~contenttype:"text/javascript" ~a:[a_src (uri_of_string "http://code.jquery.com/jquery-latest.js")] (pcdata "");
            script ~contenttype:"text/javascript" ~a:[a_src (uri_of_string (basename^".js"))] (pcdata "");
	    meta ~content:"text/html;charset=utf-8" ~a:[a_http_equiv "Content-Type"] ();
           ])
        (body [h1 [pcdata page_title];
               div ~a:[a_id "navbar"]
                 [
                   a_link "http://wiki.debian.org/Teams/OCamlTaskForce" "Home";
                   pcdata " of the ";
                   em [pcdata "OCaml Task Force"];
                   pcdata " on ";
                   a ~a:[a_href (uri_of_string "http://wiki.debian.org")] [tt [pcdata "wiki.d.o"]];
                   pcdata ".";
                 ];
               div
                 [
                   pcdata "Filter by status: ";
                   input ~a:[a_input_type `Checkbox; a_checked `Checked; a_id "good"] ();
                   pcdata "good ";
                   input ~a:[a_input_type `Checkbox; a_checked `Checked; a_id "bad"] ();
                   pcdata "bad ";
                   input ~a:[a_input_type `Checkbox; a_id "unknown"] (); pcdata "unknown";
                   span ~a:[a_id "count"] [];
                   br ();
                   input ~a:[a_input_type `Checkbox; a_checked `Checked; a_id "hide_all_ok"] ();
                   pcdata "hide fully (re-)built packages";
                   br ();
                   a_link "ocaml_transition_binNMU_request.txt" "BinNMU request";
                   pcdata " for all bad source packages."
                 ];
               div ~a:[a_class ["status"]] [summary];
               div ~a:[a_class ["footer"]] footer])

      let generate chan = pretty_print (fun s -> fprintf chan "%s%!" s) html
    end in

    with_out_file (basename^".html") XHTML.generate;
    progress "\nGenerating binNMU request...%!";

    let module BinNMU = struct
      let reason = "Recompile with OCaml "^ocaml_version

      let sources_binNMU = M.map
        (fun pkg -> List.map (fun x -> get_binNMU_number_on_arch x pkg) binaries)
        sources
      let summary_binNMU = M.map get_next_binNMU_number sources_binNMU

      let next_version_map = M.map
        (fun x ->
           let sver = (M.find x sources).sversion in
           if M.find x summary_status = Outdated then
             sprintf "%s+b%d" sver (M.find x summary_binNMU)
           else sver)
        src_of_bin_map

      let generate chan =
        let format_package pkg =
          let bad_architectures =
            List.map fst
              (List.filter (fun (_, status) -> status = Outdated)
                 (List.combine architectures (M.find pkg sources_status)))
          in
          let archs = String.concat " " bad_architectures in
          let archs = if archs = all_architectures then "ALL" else archs in
          let src = M.find pkg sources in
          let dep_waits = List.fold_left
            (fun accu dep ->
               try
                 (sprintf "%s (>= %s)" dep (M.find dep next_version_map))::accu
               with
                 | Not_found -> accu)
            [] src.sdeps
          in
          let dep_waits = sprintf "dw %s_%s . %s . -m '%s'" pkg src.sversion archs (String.concat ", " dep_waits) in
          fprintf chan "nmu %d %s_%s . %s . -m '%s'\n%s\n"
            (M.find pkg summary_binNMU)
            pkg
            src.sversion
            archs
            reason
            dep_waits
        in
        let format_section n section =
          let section = List.filter (fun x -> M.find x summary_status = Outdated) section in
          if section <> [] then begin
            fprintf chan "\n# Dependency level %d: %d outdated source package(s)\n" n (List.length section);
            List.iter format_package section;
          end
        in
        fprintf chan "# Automatically generated on %s\n" date;
        list_iteri format_section sections
    end in

    with_out_file "ocaml_transition_binNMU_request.txt" BinNMU.generate;
    progress "\n%!"

  let _ =
    let speclist = [
      "--skip-download", Arg.Set skip_download, "Skip downloading package list files";
      "--use-cache", Arg.Set use_cache, "Load marshalled package informations";
      "--quiet", Arg.Set quiet_mode, "Quiet mode";
    ] in
    Arg.parse speclist (fun s -> raise (Arg.Bad s)) "Generates ocaml_transition_monitor.html";
    if not !skip_download && not !use_cache && not (get_package_lists ()) then failwith "Error while downloading lists!";
    main ()
}
