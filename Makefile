PKGS := -package ocsigen.xhtml,str,ocamlgraph
OCAMLBUILD := ocamlbuild -ocamlc 'ocamlfind ocamlc $(PKGS)' -ocamlopt 'ocamlfind ocamlopt $(PKGS)'
OCAMLBUILD += -lflags -linkpkg -no-links

all:
	$(OCAMLBUILD) ocaml_transition_monitor.native
	for u in $(wildcard *.js *.css); do ln -sf ../$$u _build/$$u; done

cron:
	@cd _build && ./ocaml_transition_monitor.native --quiet

clean:
	$(OCAMLBUILD) -clean
